import argparse
from PIL import Image
import os
from glob import glob

def resize_image_to_fit(img, target_width, target_height, standard_ratio):
    # 调整图片大小以适应目标区域，但不拉伸图片，基于第一张图片的比例（standard_ratio）
    target_ratio = target_width / target_height
    if standard_ratio > target_ratio:
        # 基于宽度调整
        new_width = target_width
        new_height = round(target_width / standard_ratio)
    else:
        # 基于高度调整
        new_height = target_height
        new_width = round(target_height * standard_ratio)
    return img.resize((new_width, new_height), Image.ANTIALIAS), new_width, new_height

def create_collages(image_path, cols, rows, border=10, border_color='black'):
    bg_width = 4000  # 背景图宽度固定为3000像素
    images = sorted(glob(image_path + '/*.jpg') + glob(image_path + '/*.png'))
    
    if not images:
        print("No images found.")
        return
    
    # 使用第一张图片的比例作为标准
    first_img = Image.open(images[0])
    standard_ratio = first_img.width / first_img.height

    # 计算单张图片宽度和黑边宽度
    photo_width = (bg_width - border * (cols + 1)) // cols
    photo_height = int(photo_width / standard_ratio)

    # 创建输出目录
    output_dir = os.path.join(image_path, "collages")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in range(0, len(images), cols*rows):
        # 创建背景图
        bg_height = (photo_height + border) * rows + border
        background = Image.new('RGB', (bg_width, bg_height), border_color)
        
        for j, img_path in enumerate(images[i:i+cols*rows]):
            img = Image.open(img_path)
            resized_img, new_width, new_height = resize_image_to_fit(img, photo_width, photo_height, standard_ratio)
            
            x = border + (photo_width + border) * (j % cols) + (photo_width - new_width) // 2
            y = border + (photo_height + border) * (j // cols) + (photo_height - new_height) // 2
            background.paste(resized_img, (x, y))
        
        collage_path = os.path.join(output_dir, f"collage_{i // (cols*rows) + 1}.jpg")
        background.save(collage_path)
        print(f"Collage saved to {collage_path}")

def main():
    parser = argparse.ArgumentParser(description='Create collages from images in a directory based on the first image ratio.')
    parser.add_argument('image_path', type=str, help='Directory containing images')
    parser.add_argument('--cols', type=int, required=True, help='Number of columns in the collage')
    parser.add_argument('--rows', type=int, required=True, help='Number of rows in the collage')
    parser.add_argument('--border', type=int, default=10, help='Border width around each image (default: 10)')
    parser.add_argument('--color', type=str, default='black', choices=['black', 'white'], help='Border color (default: black)')

    args = parser.parse_args()

    create_collages(args.image_path, args.cols, args.rows, args.border, args.color)

if __name__ == '__main__':
    main()
