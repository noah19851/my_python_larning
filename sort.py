import os
from PIL import Image
from shutil import move

def organize_photos_by_year_month(folder_path):
    for filename in os.listdir(folder_path):
        file_path = os.path.join(folder_path, filename)
        if os.path.isfile(file_path):
            try:
                img = Image.open(file_path)
                exif_data = img._getexif()

                # EXIF数据中，拍摄时间的标签是36867
                if 36867 in exif_data:
                    shot_year_month = exif_data[36867][:7].replace(':', '/')
                    new_folder = os.path.join(folder_path, shot_year_month)

                    if not os.path.exists(new_folder):
                        os.makedirs(new_folder)

                    new_path = os.path.join(new_folder, filename)
                    move(file_path, new_path)
            except:
                # 如果文件不是图片或者没有EXIF数据，就直接忽略
                pass

# 使用方法
organize_photos_by_year_month('/Users/Noah/Pictures/watermark')