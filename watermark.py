from datetime import datetime
from statistics import mode
from sys import argv
import os
import sys
from PIL import Image, ImageDraw, ImageFont
from image_tools import resize_image, add_canvas_to_image, read_image_metadata, rotate_and_flip_image
import concurrent.futures
import piexif

LOGO_HEIGH = 100
TEXT_PADDING = 12
EXIF_INFO_SIZE = 26
COPYRIGHT_SIZE = 36
FONT_COLOR = (60, 60, 60)
FONT_COLOR_UNDER = (100, 100, 100)
EXIF_FONT = "Arial Bold.ttf"
EXIF_FONT_UNDER = "Arial.ttf"
COPYRIGHT_FONT = EXIF_FONT
TARGET_PATH = "/Users/Noah/Pictures/watermark/"
SHARING_PATH = "/Users/Noah/Pictures/分享输出/"

# COPYRIGHT = f"©Noah {datetime.now().year}"
LOGO_MAPPING = {
        "NIKON CORPORATION": f"{sys.path[0]}/logo/nikon_logo.jpg",
             "FUJIFILM": f"{sys.path[0]}/logo/fuji_logo.jpg",
             "RICOH IMAGING COMPANY, LTD.": f"{sys.path[0]}/logo/ricoh_logo.jpg",
             "Apple": f"{sys.path[0]}/logo/apple.png",
             "DJI": f"{sys.path[0]}/logo/DJI.jpg",
                }

def make_watermark(image_path):
    if not os.path.isdir(TARGET_PATH):
        os.makedirs(TARGET_PATH)
    fileName = os.path.basename(image_path)

    img_meta_data = read_image_metadata(image_path)
    year = img_meta_data['date_time_year']

    # resize image
    image = rotate_and_flip_image(Image.open(image_path), img_meta_data["orientation"])

    width, height = image.size
    # 缩放到指定大小
    if max(width, height) > 3000:
        if width > height:
            new_width = 3000
            new_height = int(height * new_width / width)
        else:
            new_height = 3000
            new_width = int(width * new_height / height)
        image = image.resize((new_width, new_height), resample=Image.LANCZOS)

    canvas_width = image.width + 160
    canvas_height = image.height + 400
    image_x = 80
    image_y = 80

    # 创建新的画布 
    canvas = Image.new('RGB', (canvas_width, canvas_height), color='white')

    # 将原始图片粘贴到画布中心
    canvas.paste(image, (image_x, image_y))

    # logo
    make = img_meta_data['camera_make'].strip()
    print(make)
    logoPath = LOGO_MAPPING[make]
    logo = makeLogo(logoPath)
    logo_bottom = 0
    if logo:
        x = (canvas.width - logo.width) // 2
        y = 160 + image.height
        logo_bottom = y + logo.height
        canvas.paste(logo, (x, y - 8))


    camera_make = img_meta_data['camera_make'].lstrip().rstrip()
    camera_model = img_meta_data['camera_model'].lstrip().rstrip()
    if camera_model == "PP-101":
        camera_model = "Osmo Pocket 3"
    lens = img_meta_data['lens_info'].replace("Unknown", "").lstrip()
    if make == "Apple" or camera_model == "X100VI":
        lens = ""

    if camera_model.upper() == lens.upper() or f"{camera_make.upper()} {camera_model.upper()}" == lens.upper() : lens = ""
    camera_text = camera_model
    if "f/--" not in lens and (lens and lens.strip()):
        camera_text = f"{camera_text} & {lens}"
    camera_info = f"shot on {camera_text}".replace(" NIKON", "").replace("Fujifilm Fujinon ", "").replace(" RICOH", "").replace("Unknown", "").lstrip().replace("\0", "")
    COPYRIGHT = f"©Noah {year}"
    
    info_text = f"{camera_info} | {COPYRIGHT}"
    print(info_text)
    font = ImageFont.truetype(font="Tahoma", size=48)
    draw = ImageDraw.Draw(canvas)
    
    text_width, _ = draw.textsize(info_text, font=font)
    draw.text(xy=((canvas.width - text_width) / 2, logo_bottom),
              text=info_text, fill=(80,80,80), font=font)

    exif_dict = {"0th":{}, "Exif":{}, "GPS":{}, "1st":{}, "thumbnail":None, "Interop":{}}
    exif_dict["Exif"] = img_meta_data['exif']
    exif_dict["0th"] = img_meta_data['0th']
    # 检查并删除Orientation标签
    if 274 in exif_dict["0th"]:
        del exif_dict["0th"][274]

    exif_bytes = piexif.dump(exif_dict)

    fullPath = f"{TARGET_PATH}/{fileName}"
    canvas.save(fullPath, quality=99, subsampling=0, exif=exif_bytes)

    print(f"{fullPath} done")


def makeWatermark(image_path):

    if not os.path.isdir(TARGET_PATH):
        os.makedirs(TARGET_PATH)

    fileName = os.path.basename(image_path)

    img_meta_data = read_image_metadata(image_path)
    print(img_meta_data)
    (canvas_img, resized_image) = add_canvas_to_image(image_path, 1.1)

    camera_make = img_meta_data['camera_make']
    camera_model = img_meta_data['camera_model']
    lens = img_meta_data['lens_info']
    if camera_model.upper() == lens.upper() or f"{camera_make.upper()} {camera_model.upper()}" == lens.upper() : lens = ""
    camera_info = f"{camera_make} {camera_model} {lens} ".replace("NIKON CORPORATION NIKON", "NIKON").replace("Fujifilm Fujinon", "").replace("RICOH IMAGING COMPANY, LTD.", "").replace("Unknown", "").lstrip()
    shutting_params = f"{img_meta_data['focal_length']}mm F{img_meta_data['aperture']}  {img_meta_data['shutter_speed']}s  ISO{img_meta_data['iso']}"

    text_x = int((canvas_img.width - resized_image.width) // 2) 
    text_y = int(resized_image.height +  (canvas_img.height - resized_image.height) // 2) + 8

    font = ImageFont.truetype(font=EXIF_FONT, size=EXIF_INFO_SIZE)
    fontUnder = ImageFont.truetype(font=EXIF_FONT, size=EXIF_INFO_SIZE)
    font2 = ImageFont.truetype(font=COPYRIGHT_FONT, size=COPYRIGHT_SIZE)

    draw = ImageDraw.Draw(canvas_img)
    draw.text(xy=(text_x, text_y),
              text=camera_info, fill=FONT_COLOR, font=font)
    draw.text(xy=(text_x, text_y + EXIF_INFO_SIZE + 4),
              text=shutting_params, fill=FONT_COLOR_UNDER, font=fontUnder)
    year = img_meta_data['date_time_year']
    month = img_meta_data['date_time_month']
    COPYRIGHT = f"©Noah {year}"
    textSize = draw.textsize(text=COPYRIGHT, font=font2)
    draw.text(xy=((text_x + resized_image.width) - textSize[0], text_y),
              text=COPYRIGHT, fill=FONT_COLOR, font=font2)
    
    make = img_meta_data['camera_make'].strip()
    logoPath = LOGO_MAPPING[make]
    logo = makeLogo(logoPath)
    if logo:
        x = (canvas_img.width - logo.width) // 2
        y = text_y
        canvas_img.paste(logo, (x, y - 8))

    exif_dict = {"0th":{}, "Exif":{}, "GPS":{}, "1st":{}, "thumbnail":None, "Interop":{}}
    # date = img_meta_data['date_time']

    # exif_dict["0th"][piexif.ImageIFD.DateTime] = date
    # exif_dict["Exif"][piexif.ExifIFD.DateTimeOriginal] = date
    exif_dict["Exif"] = img_meta_data['exif']
    exif_dict["0th"] = img_meta_data['0th']
    exif_bytes = piexif.dump(exif_dict)

    targetPath = f"{TARGET_PATH}{year}/{month}"
    if not os.path.isdir(targetPath):
        os.makedirs(targetPath)

    fullPath = f"{targetPath}/{fileName}"
    canvas_img.save(fullPath, quality=99, subsampling=0, exif=exif_bytes)

    print(f"{fullPath} done")


def makeLogo(logo_path):

    if logo_path and os.path.exists(logo_path):
        logo = Image.open(logo_path)
        logoRadio = logo.width / logo.height
        return logo.resize((int(LOGO_HEIGH * logoRadio), LOGO_HEIGH))
    return None


def getImageFile(fileName):
    if fileName.lower().endswith('.jpg'):
        return [fileName]
    imageList = []
    for parent, _, filenames in os.walk(fileName):
        for name in filenames:
            if name.lower().endswith('.jpg'):
                imageList.append(os.path.join(parent, name))
    return imageList



if __name__ == '__main__':
    start = datetime.now()
    imagePaths = getImageFile(argv[1])
    # 创建一个 ThreadPoolExecutor
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # 使用 executor.map 函数将 makeWatermark 函数应用到 imagePaths 的每一个元素
        executor.map(make_watermark, imagePaths)

    end = datetime.now()
    print(end - start)
